﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;

//should be synced to the class in Python-side code
const LabeledSentenceSchema = new Schema({
    _id: Number,
    sentence: { type: String, maxlength: 100 },
    label: { type: Number, min: 0, required: true },
    name: { type: String, required: true }
}, { collection: 'labeled_sentence', versionKey: false });

module.exports = require('../connection').model('LabeledSentence', LabeledSentenceSchema);