﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;

const CustomContextSchema = new Schema({
    user: Schema.Types.Mixed,
    parent: Schema.Types.Mixed,
    name: String,
    dialogset: { type: Schema.Types.Mixed, ref: 'DialogSet' }
});

module.exports = require('../mb-connection')
    .model('CustomContext', CustomContextSchema);