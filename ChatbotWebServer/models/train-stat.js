﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;

//should be synced to the class in Python-side code
const TrainStatSchema = new Schema({
    name: { type: String, maxlength: 20, index: { unique: true }, unique: true },
    numEpochs: { type: Number, min: 1, default: 1 },
    finished: { type: Boolean, default: false }
}, { collection: 'train_stat', versionKey: false });

module.exports = require('../connection').model('TrainStat', TrainStatSchema);