﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;

const DialogSetDialogSchema = new Schema({
    input: Schema.Types.Mixed,
    id: Number,
    inputRaw: Schema.Types.Mixed,
    context: { type: Schema.Types.Mixed, ref: 'CustomContext' },
    output: Schema.Types.Mixed,
    dialogset: { type: Schema.Types.Mixed, ref: 'DialogSet' },
    tag: Array
});

module.exports = require('../mb-connection')
    .model('DialogSetDialog', DialogSetDialogSchema);