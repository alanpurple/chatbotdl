﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const notiSchema = new Schema({
    message: { type: String, required: true },
    link: String,
    read: { type: Boolean, default: false }
});

const AccountTypes = 'admin local google facebook'.split(' ');

// email is the actual(logical) primary key(not physical, there exists _id inside)
const UserSchema = new Schema({
    _id: Number,
    email: { type: String, index: { unique: true }, unique: true },
    emailVerified: { type: Boolean, default: false },
    nickName: String,
    oauthId: Number,
    password: String,
    birthday: Date,
    accountType: { type: String, required: true, enum: AccountTypes },
    hasCustomThumbnail: { type: Boolean, default: false },
    dateVerifySent: { type: Date, expires: 60 * 3 }, // email verification is valid only for 3 minutes
    noti: [notiSchema],
    dateSigned: { type: Date, default: new Date() }
});

UserSchema.pre('save', function (next) {
    const user = this;
    if (user.accountType == 'local' || user.accountType == 'admin') {
        if (user.oauthId) {
            var error = new Error('oAuth not null but type is local.');
            next(error);
        }
        else {
            if (!user.email) {
                var error = new Error('local account must have a valid email address');
                next(error);
            }
            else {
                // hash the password only if the password has been changed or user is new
                if (!user.isModified('password')) next();
                else {
                    bcrypt.genSalt(function (err, salt) {
                        bcrypt.hash(user.password, salt, function (err, hash) {
                            if (err) {
                                console.error(err);
                                next(err);
                            }
                            else {
                                user.password = hash;
                                next();
                            }
                        });
                    });
                }
            }
        }
    }
    else {
        if (!user.oauthId || !user.email)
            next(new Error('Invalid OAuth2.0 account'));
        else
            next();
    }
});

UserSchema.methods.comparePassword = function (password, callback) {
    const user = this;
    bcrypt.compare(password, user.password, function (err, result) {
        if (err) {
            console.error(err);
            callback(null, false);
        }
        else if (!result)
            callback(null, false);
        else
            callback(null, user);
    });
};

module.exports = require('../connection').model('User', UserSchema);