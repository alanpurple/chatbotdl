﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;

const DialogSetSchema = new Schema({
    language: String,
    content: String,
    title: String,
    usable: Boolean,
    path: String,
    filename: String
});

module.exports = require('../mb-connection')
    .model('DialogSet', DialogSetSchema);