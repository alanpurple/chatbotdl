﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;

const CounterSchema = new Schema({
    _id: String,
    seq: { type: Number, default: 0 }
});

module.exports = require('../connection').model('Counter', CounterSchema);
