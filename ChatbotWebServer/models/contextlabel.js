﻿const mongoose = require('../set-mongoose');
const Schema = mongoose.Schema;

const ContextLabelSchema = new Schema({
    _id: Number,
    label: { type: String, unique: true }
});

module.exports = require('../connection')
    .model('ContextLabel', ContextLabelSchema);