﻿const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

const User = require('./models/user');
const Counter = require('./models/counter');

passport.serializeUser(function (user, done) {
    done(null, user.email);
});

passport.deserializeUser(function (em, done) {
    User.findOne({ email: em }, function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy(
    function (emailAddr, password, done) {
        User.findOne({ email: emailAddr }, function (err, user) {
            if (err)
                return done(err);
            else if (!user) {
                Counter.findById('user', function (err, cnt) {
                    if (err)
                        return done(err);
                    else {
                        let user = new User({
                            _id: cnt.seq,
                            email: emailAddr,
                            password: password,
                            accountType: 'local'
                        });
                        user.save(err => {
                            if (err)
                                return done(err);
                            else {
                                cnt.seq++;
                                cnt.save(err => {
                                    if (err)
                                        return done(err);
                                    else
                                        return done(null, user);
                                });
                            }
                        });
                    }
                });
            }
            else
                user.comparePassword(password, done);
        });
    }
));

passport.use(new GoogleStrategy({
    // local test id and secret
    clientID: '567723322080-j2k91vbqbvbtlkss45vtkaou30r4kogf.apps.googleusercontent.com',
    clientSecret: 'mDljWHVSiXYsvu_DvTaCuJQq',
    callbackURL: 'http://localhost:1337/auth/google/callback'
},
function (accessToken, refreshToken, profile, done) {
    User.findOne({ oauthId: profile.id }, function (err, user) {
        if (err)
            return done(err);
        else if (user)
            return done(null, user);
        else {
            if (!profile.emails)
                return done(new Error('cannot recognize email'), user);
            else {
                Counter.findById('user', function (err, cnt) {
                    if (err)
                        return done(err);
                    else {
                        var user = new User({
                            _id:cnt.seq,
                            email: profile.emails[0].value,
                            emailVerified: true,
                            infoInitialized: false,
                            oauthId: profile.id,
                            accountType: 'google'
                        });
                        user.save(error => {
                            if (error) {
                                console.error(error);
                                return done(error, null);
                            }
                            else {
                                cnt.seq++;
                                cnt.save(err => {
                                    if (err)
                                        return done(err);
                                    else
                                        return done(null, user);
                                });
                            }
                        });
                    }
                });
            }
        }
    });
}
));

passport.use(new FacebookStrategy({
    // local test id and secret
    // these are temporary values
    clientID: '1714693575458575',
    clientSecret: '24411feb5f4e6d8a5c01b85d111afa1a',
    callbackURL: 'http://localhost:1337/auth/facebook/callback',
    profileFields: ['displayName','name', 'friendlists', 'email']
},
function (accessToken, refreshToken, profile, done) {
    User.findOne({ oauthId: profile.id }, (err, user) => {
        if (err) {
            console.error(err);
            return done(err, user);
        }
        else if (user)
            return done(null, user);
        else {
            if (!profile.emails)
                return done(new Error('cannot recognize email'), user);
            else {
                Counter.findById('user', function (err, cnt) {
                    if (err)
                        return done(err);
                    else {
                        const user = new User({
                            email: profile.emails[0].value,
                            emailVerified: true,
                            infoInitialized: false,
                            oauthId: profile.id,
                            accountType: 'facebook'
                        });
                        user.save(error => {
                            if (error) {
                                console.error(error);
                                return done(error, null);
                            }
                            else {
                                cnt.seq++;
                                cnt.save(err => {
                                    if (err)
                                        return done(err);
                                    else
                                        return done(null, user);
                                });
                            }
                        });
                    }
                });
            }
        }
    });
}
));

module.exports = passport;