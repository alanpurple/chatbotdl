﻿const router = require('express').Router();
const passport = require('passport');
const nodeMailer = require('nodemailer');
const path = require('path');
const multer = require('multer');
const fs = require('fs');

const User = require('../models/user');

router.get('/logout', ensureAuthenticated, function (req, res) {
    req.logout();
    res.redirect('/');
});

router.get('/info', function (req, res) {
    if (req.isAuthenticated())
        res.send(req.user);
    else
        res.sendStatus(401);
});

router.get('/checkLogin', function (req, res) {
    let result;
    let hasNickName = false;
    if (req.isAuthenticated()) {
        result = true;
        if (req.user.nickName)
            hasNickName = true;
    }
    else
        result = false;
    res.send({
        result: result,
        hasNickName: hasNickName
    });
});

router.get('/notLoggedIn', function (req, res) {
    if (req.isAuthenticated())
        res.sendStatus(400);
    else
        res.send('not logged in');
});

//Authenticate Admin account
router.get('/admin', ensureAuthenticated, function (req, res) {
    if (req.user.accountType === 'admin')
        res.sendStatus(200);
    else
        res.sendStatus(401);
});

router.get('/checkNickName/:nickName', function (req, res) {
    User.findOne({ nickName: req.params.nickName })
        .exec(function (err, user) {
            if (err) {
                console.error(err);
                res.sendStatus(500);
            }
            else if (user)
                res.send('DUPLICATE');
            else
                res.send('AVAILABLE');
        });
});

router.get('/checkUser/:id', function (req, res) {
    User.findOne({ email: decodeURI(req.params.id) }, function (err, user) {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (!user)
            res.sendStatus(404);
        else
            res.send('User already exists');
    });
});

router.put('/', ensureAuthenticated, function (req, res) {
    User.findOne({ email: req.user.email }).exec(
        function (err, user) {
            if (err || !user)
                res.sendStatus(500);
            else {
                for (var prop in req.body) {
                    if (prop === 'email') {
                        console.error('email cannot be modified');
                        res.sendStatus(500);
                        return;
                    }
                    else if (prop === 'nickName' && user.nickName) {
                        console.error('nickName already initialized and cannot be modified');
                        res.sendStatus(500);
                        return;
                    }
                    else
                        user[prop] = req.body[prop];
                }
                user.save(err => {
                    if (err) {
                        console.error(err);
                        res.sendStatus(500);
                    }
                    else
                        res.send('User info updated successfully');
                });
            }
        });
});

router.post('/login',
    function (req, res, next) {
        User.findOne({ email: req.body.username }, function (err, user) {
            if (err) {
                console.error(err);
                res.redirect('/');
            }
            else if (!user)
                res.redirect('/signup');
            else
                next();
        });
    },
    passport.authenticate('local', { failureRedirect: '/login' }),
    function (req, res) {
        if (!req.user.emailVerified)
            res.redirect('/emailVerification');
        else if (!req.user.nickName)
            res.redirect('/user-info');
        //else if (req.user.accountType == 'admin')
        //    res.redirect('/admin');
        else
            res.redirect('/');
    });

router.post('/signup',
    function (req, res, next) {
        User.findOne({ email: req.body.username }, function (err, user) {
            if (err) {
                console.error(err);
                res.redirect('/');
            }
            else if (!user)
                next();
            else
                res.redirect('/login/' + encodeURI(req.body.username));
        });
    },
    passport.authenticate('local', {
        failureRedirect: '/signup'
    }),
    function (req, res) {
        res.redirect('/user-info');
    });

// This parameters should be set properly for email-send function
const USERNAME = 'wsoh@wise.co.kr';
//Dummy data
const CLIENT_ID = '000000';
const CLIENT_SECRET = 'aaaaa';
const REFRESH_TOKEN = 'not decided';
const ACCESS_TOKEN = 'not retreived yet';

var generator = require('xoauth2').createXOAuth2Generator({
    user: USERNAME,
    clientID: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    refreshToken: REFRESH_TOKEN,
    accessToken: ACCESS_TOKEN
});

var transporter = nodeMailer.createTransport({
    service: 'gmail',
    auth: {
        xoauth2: generator
    }
});

router.get('/sendVerifyEmail', function (req, res) {
    if (!req.isAuthenticated()) {
        res.sendStatus(401);
        return;
    }
    else if (req.user.accountType !== 'local' || req.user.emailVerified) {
        console.log('send verification mail api is for local and unverified accounts');
        res.sendStatus(400);
        return;
    }
    User.findOne({ email: req.user.email }).exec(
        function (err, user) {
            if (err) {
                console.error(err);
                res.sendStatus(500);
                return;
            }
            if (!user) {
                res.sendStatus(404);
                return;
            }
            user.tempId = Math.floor(Math.random() * 100 + 54);
            user.dateVerifySent = Date.now();
            user.save(function (err) {
                //link to email verification process page, should be set to actual web service address later
                var link = 'http://' + req.hostname + '/emailVerifyReturn/'
                    + req.user.email + '/' + user.tempId;
                transporter.sendMail({
                    from: 'alan@moneybrain.ai',
                    to: req.user.email,
                    subject: "이메일 확인.",
                    html: "Chatbot 계정 확인 메일입니다."
                    + "직접 가입하셨다면 클릭해주십시요.<br><a href=" + link + ">내 계정 활성화</a>"
                }, function (err, info) {
                    if (err) {
                        console.error(err);
                        res.sendStatus(500);
                    }
                    else
                        res.send('message sent');
                });
            });
        }
    );
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    else res.sendStatus(401);
}

module.exports = router;