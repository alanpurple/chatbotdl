﻿const router = require('express').Router();
const LabelSentence = require('../models/label-sentence');
const TrainStat = require('../models/train-stat');
const Counter = require('../models/counter');
const PROTO_PATH = __dirname + '/../../DLServer/chatbotservice.proto';
const grpc = require('grpc');
const chatbotservice_proto = grpc.load(PROTO_PATH).ChatbotService;

const client = new chatbotservice_proto.ChatbotService('localhost:50051',
    grpc.credentials.createInsecure());

router.get('/clf', (req, res) =>
    LabelSentence.find((err, data) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (!data)
            res.sendStatus(404);
        else
            res.send(data);
    }));

router.get('/clf/list', (req, res) =>
    LabelSentence.find({},'name').then(arr => {
        names = [];
        arr.forEach(elem => {
            if (!names.includes(elem.name))
                names.push(elem.name);
        });
        res.send(names);
    }).catch(err => {
        console.error(err);
        res.sendStatus(500);
    })
);

router.post('/clf', (req, res) => {
    if (!req.body) {
        res.sendStatus(401);
        return;
    }
    if (!req.body.length) {
        res.sendStatus(401);
        return;
    }
    if (req.body.length < 1) {
        res.sendStatus(401);
        return;
    }
    Counter.findById('labeledsentence', (err, cnt) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
            return;
        }
        if (!cnt) {
            cnt = new Counter({ _id: 'labeledsentence' });
        }
        let lsArr = [];
        let idArr = [];
        req.body.forEach((elem, index) => {
            idArr.push[cnt.seq];
            let temp = new LabelSentence({
                _id: cnt.seq++,
                sentence: elem.sentence,
                label: elem.label,
                name:elem.name
            });
            lsArr.push(temp);
        });
        LabelSentence.create(lsArr, err => {
            if (err) {
                console.error(err);
                res.sendStatus(500);
            }
            else
                cnt.save(err => {
                    if (err) {
                        console.error(err);
                        res.sendStatus(500);
                    }
                    else
                        res.send(idArr);
                });
        });
    });
});

router.put('/clf/:id', (req, res) =>
    LabelSentence.findById(req.params.id, (err, data) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else {
            for (let prop in req.body)
                data[prop] = req.body[prop];
            data.save(err => {
                if (err) {
                    console.error(err);
                    res.sendStatus(500);
                }
                else
                    res.send('item updated successfully');
            });
        }
    }));

router.delete('/clf/:id', (req, res) => {
    LabelSentence.findByIdAndRemove(req.params.id, err => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else
            res.send('deletion completed');
    });
});

router.get('/clf/train', (req, res) =>
    TrainStat.find((err, data) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (!data)
            res.sendStatus(404);
        else
            res.send(data);
    }));

router.get('/clf/:name', (req, res) =>
    LabelSentence.find({ name: req.params.name }, (err, data) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (!data)
            res.sendStatus(404);
        else
            res.send(data);
    }));



router.post('/clf/train', (req, res) => {
    client.clfTrain(req.body, (err, result) => {
        if (err)
            console.error(err);
        else {
            switch (result.error) {
                case -1:
                    console.log('Training started');
                    break;
                case 0:
                    console.log('The training ' + req.body.name +
                        ' has error caused by invalid input');
                    break;
                case 1: default:
                    console.log('The training ' + req.body.name +
                        ' has some process error, check the python server log');
                    break;
            }
        }
    });
    res.send('Training started');
});

router.get('/clf/train/:name', (req, res) => {
    TrainStat.findOne({ name: req.params.name }, (err, stat) => {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else if (!stat)
            res.sendStatus(404);
        else
            res.send(stat);
    });
});

router.post('/clf/predict', (req, res) => {
    client.textClf(req.body, (err, result) => {
        if (err)
            console.error(err);
        else {
            switch (result.error) {
                case -1:
                    res.send(result.probability);
                    break;
                case 0:
                    res.sendStatus(401);
                    break;
                case 1: default:
                    res.sendStatus(500);
                    break;
            }
        }
    });
});

module.exports = router;