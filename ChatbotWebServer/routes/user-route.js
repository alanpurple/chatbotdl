﻿const Router = require('express').Router();

const User = require('../models/user');

Router.get('/', ensureAdmin, function (req, res) {
    User.find({}, function (err, users) {
        if (err) {
            console.error(err);
            res.sendStatus(500);
        }
        else
            res.send(users);
    });
});

function ensureAdmin(req, res, next) {
    if (!req.isAuthenticated())
        res.sendStatus(401);
    else if (req.user.accountType != admin)
        res.sendStatus(401);
    else
        next();
}

module.exports = Router;