﻿const mongoose = require('./set-mongoose');
const Counter = require('./models/counter');
const LabelSentence = require('./models/label-sentence');
const DialogSet = require('./models/dialogset');
const DialogSetDialog = require('./models/dialogsetdialog');
const CustomContext = require('./models/customcontext');
const ContextLabel = require('./models/contextlabel');

function importOnlyOnce() {

    ContextLabel.find().then(data => {
        if (data.length > 0) {
            console.log('labels exist, skip mb-data import');
            return;
        }
        return DialogSetDialog.find({
            context: { $exists: true, $ne: null, $type: 'objectId' },
            input: { $type: 'string' }
        }, 'input context').populate('context')
            .then(dialogs => {
                return Counter.find({ _id: { $in: ['contextlabel', 'labeledsentence'] } })
                    .then(cnts => {
                        lscnt = cnts.find(elem => elem._id == 'labeledsentence');
                        cnt = cnts.find(elem => elem._id == 'contextlabel');
                        if (!cnt)
                            cnt = new Counter({ _id: 'contextlabel', seq: 0 });
                        if (!lscnt)
                            lscnt = new Counter({ _id: 'labeledsentence' });
                        let labels = [];
                        let clfData = [];
                        dialogs.forEach(dialog => {
                            let labelId = labels.findIndex(elem =>
                                elem.label === dialog.context.name);
                            if (labelId < 0) {
                                labels.push(new ContextLabel({
                                    _id: cnt.seq++,
                                    label: dialog.context.name
                                }));
                                labelId = labels.length - 1;
                            }
                            clfData.push(new LabelSentence({
                                _id: lscnt.seq++,
                                label: labelId,
                                sentence: dialog.input,
                                name: 'mb-data'
                            }));
                        });
                        cnt.save().catch(err => console.error(err));
                        lscnt.save().catch(err => console.error(err));
                        ContextLabel.create(labels)
                            .then(() => console.log('label dictionary updated'))
                            .catch(err => {
                                console.error(err);
                                console.error('error while creating label dictionary');
                            });
                        return LabelSentence.create(clfData)
                            .then(() => console.log('import from md-data successful'));
                    });
            });
    }).catch(err => console.error(err));
}

module.exports = importOnlyOnce;