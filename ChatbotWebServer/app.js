﻿var debug = require('debug');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

var app = express();

const rootPath = path.join(__dirname, '../ChatbotDemo/wwwroot');

app.use(favicon(__dirname + '/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

////////////////////////////////////
// Settings for single-page front-end routing
////////////////////////////////////
app.get(['/','/text-classification','/admin/?', '/user-info/?',
    '/signup/?', '/login', '/login/*'],
    function (req, res) {
        res.sendFile(path.join(rootPath, 'index.html'));
    });
// End of front-end routing
///////////////////////////
app.use(express.static(rootPath, { index: false }));

app.use(session({
    secret: 'do not need to know',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: require('./connection') })
}));

const myPassport = require('./passport-init');

app.use(myPassport.initialize());
app.use(myPassport.session());

const Counter = require('./models/counter');
const User = require('./models/user');

// Initial settings for counter(for user) and default admin for test(wsoh@wise.co.kr)
Counter.findById('user', function (err, cnt) {
    if (err)
        console.error(err);
    else if (!cnt) {
        const userCnt = new Counter({
            _id: 'user',
            seq: 2
        });

        User.findById(1, function (err, user) {
            if (err)
                console.error(err);
            else if (!user) {
                const admin = new User({
                    _id: 1,
                    email: 'alan@moneybrain.ai',
                    emailVerified: true,
                    nickName: 'alan racer',
                    accountType: 'admin',
                    password: 'testadmin'
                });
                admin.save(err => {
                    if (err)
                        console.error(err);
                    else
                        userCnt.save(err => {
                            if (err)
                                console.error(err);
                        });
                });
            }
        });
    }
    else {
        User.findById(1, function (err, user) {
            if (err)
                console.error(err);
            else if (!user)
                throw new Error('user counter exists yet there is no default admin account for test');
        });
    }
});

require('./import-mbdata')();

app.use('/dl', require('./routes/dl-route'));
app.use('/auth', require('./routes/auth-route'));
app.use('/account', require('./routes/account-route'));
app.use('/user', require('./routes/user-route'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        console.error(err);
        res.status(err.status || 500);
        res.send(err.message);
    });
}

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), () =>
    debug('Express server listening on port ' + server.address().port));