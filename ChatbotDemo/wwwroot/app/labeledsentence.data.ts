﻿export class LabelSentenceData {
    _id?: number;
    sentence: string;
    label: number;
    name: string;
}