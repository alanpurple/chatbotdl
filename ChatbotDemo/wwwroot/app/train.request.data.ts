﻿export class TrainRequestData {
    name?: string;
    type: number = 0;
    fileName?: string;
    userId?: number;
    language?: number;
    maxLength: number = 30;
    numEpochs: number = 1;
    isTokenized: boolean = true;
}