﻿import { Component, Injectable, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
    template: `
                <h3>{{msg}}</h3><br><br><b>홈으로 이동합니다.</b><br>
                <button mat-button (click)="dialogRef.close()">확인</button>
              `
})
export class ErrorDialog {
    constructor(private dialogRef: MatDialogRef<ErrorDialog>) { }

    @Input() msg: string;
}

@Injectable()
export class ErrorAlert {
    constructor(private dialog: MatDialog,
        private router: Router) {
    }

    open(msg?: string) {
        const ref = this.dialog.open(ErrorDialog);
        ref.componentInstance.msg = msg ? msg : '에러남!';
        ref.afterClosed().subscribe(() => this.router.navigate(['/']));
    }
}