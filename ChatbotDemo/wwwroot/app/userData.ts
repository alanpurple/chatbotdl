﻿export interface UserData {
    _id: number,
    email: string,
    emailVerified?: boolean,
    nickName?: string
}