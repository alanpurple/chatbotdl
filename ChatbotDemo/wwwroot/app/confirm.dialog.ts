﻿import { Component, Injectable, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
    template: `
                <h3>{{msg}}</h3><br>
                <button mat-button (click)="dialogRef.close()">확인</button>
              `
})
export class ConfirmDialogTemplate {
    constructor(private dialogRef: MatDialogRef<ConfirmDialogTemplate>) { }

    @Input() msg: string;
}

@Injectable()
export class ConfirmDialog {
    constructor(private dialog: MatDialog) { }

    // msg should be properly handed
    open(msg?: string) {
        this.dialog.open(ConfirmDialogTemplate)
            .componentInstance.msg = msg ? msg : '확인 메시지입니다';
    }
}