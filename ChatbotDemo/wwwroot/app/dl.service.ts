﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { ClfRequsetData } from './clf.request.data';
import { TrainRequestData } from './train.request.data';
import { TrainStatData } from './train.stat.data';
import { LabelSentenceData } from './labeledsentence.data';

@Injectable()
export class DlService {
    constructor(private http: HttpClient) { }

    getClfDataAll(): Observable<LabelSentenceData[]> {
        return this.http.get<LabelSentenceData[]>('/dl/clf');
    }
    getClfData(name: string): Observable<LabelSentenceData> {
        return this.http.get<LabelSentenceData>('/dl/clf/' + name);
    }
    saveClfData(data: LabelSentenceData[]): Observable<number[]> {
        return this.http.post<number[]>('/dl/clf', data);
    }
    editClfData(data: {}): Observable<boolean> {
        return this.http.put('/dl/clf/' + data['_id'], { responseType: 'Text' })
            .map(str => true);
    }
    deleteClfData(id: number): Observable<boolean> {
        return this.http.delete('/dl/clf', { responseType: 'text' })
            .map(str => true);
    }
    getTrainDataList(): Observable<string[]> {
        return this.http.get<string[]>('/dl/clf/list');
    }
    getTrainStatAll(): Observable<TrainStatData[]> {
        return this.http.get<TrainStatData[]>('/dl/clf/train');
    }
    getTrainStat(name: string): Observable<TrainStatData> {
        return this.http.get<TrainStatData>('/dl/clf/train/' + name);
    }
    trainClf(data: TrainRequestData): Observable<boolean> {
        return this.http.post('/dl/clf/train', data, { responseType: 'text' })
            .map(str => true);
    }
    predictClf(data: ClfRequsetData): Observable<number[]> {
        return this.http.post<number[]>('/dl/clf/predict', data);
    }
}