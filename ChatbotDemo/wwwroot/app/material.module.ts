﻿import { NgModule } from '@angular/core';
import {
    MatButtonModule, MatCardModule, MatCheckboxModule,
    MatChipsModule, MatDialogModule, MatGridListModule,
    MatIconModule, MatInputModule, MatRadioModule,
    MatSidenavModule, MatListModule, MatToolbarModule,
    MatMenuModule, MatSelectModule, MatDatepickerModule,
    MatNativeDateModule, MatExpansionModule, MatTableModule,
    MatSlideToggleModule, MatSliderModule, MatProgressSpinnerModule,
    MatTooltipModule, MatProgressBarModule, MatTabsModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';

@NgModule({
    imports: [
        MatButtonModule, MatCardModule, MatCheckboxModule,
        MatChipsModule, MatDialogModule, MatGridListModule,
        MatIconModule, MatInputModule, MatRadioModule,
        MatSidenavModule, MatListModule, MatToolbarModule,
        MatMenuModule, MatSelectModule, MatDatepickerModule,
        MatNativeDateModule, MatExpansionModule, MatTableModule,
        CdkTableModule, MatSlideToggleModule, MatSliderModule,
        MatProgressSpinnerModule, MatTooltipModule, MatProgressBarModule,
        MatTabsModule
    ],
    exports: [
        MatButtonModule, MatCardModule, MatCheckboxModule,
        MatChipsModule, MatDialogModule, MatGridListModule,
        MatIconModule, MatInputModule, MatRadioModule,
        MatSidenavModule, MatListModule, MatToolbarModule,
        MatMenuModule, MatSelectModule, MatDatepickerModule,
        MatNativeDateModule, MatExpansionModule, MatTableModule,
        CdkTableModule, MatSlideToggleModule, MatSliderModule,
        MatProgressSpinnerModule, MatTooltipModule, MatProgressBarModule,
        MatTabsModule
    ]
})
export class AIscMaterialModule { }