﻿import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { LabelSentenceData } from './labeledsentence.data';

@Component({
    templateUrl: './labeledsentence.dialog.html',
    moduleId: module.id
})
export class LabeledSentenceDialog {
    constructor(
        private dialogRef: MatDialogRef<LabeledSentenceDialog>
    ) { }

    private _ls: LabelSentenceData = new LabelSentenceData();
    private isNew: boolean = true;

    @Input()
    set ls(data: LabelSentenceData) {
        if (data) {
            this.isNew = false;
            this._ls = data;
        }
    }
}