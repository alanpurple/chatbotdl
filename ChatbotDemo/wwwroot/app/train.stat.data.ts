﻿export interface TrainStatData {
    name: string,
    numEpochs?: number,
    finished: boolean
}