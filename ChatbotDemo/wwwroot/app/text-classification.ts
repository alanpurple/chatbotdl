﻿import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LabelSentenceData } from './labeledsentence.data';
import { LabeledSentenceDialog } from './labeledsentence.dialog';
import { TrainStatData } from './train.stat.data';
import { ClfRequsetData } from './clf.request.data';
import { TrainRequestData } from './train.request.data';
import { DlService } from './dl.service';
import { ErrorAlert } from './error.alert';
import { ConfirmDialog } from './confirm.dialog';

@Component({
    templateUrl: './text-classification.html',
    moduleId: module.id
})
export class TextClassification implements OnInit {

    constructor(
        private dlService: DlService,
        private errorAlert: ErrorAlert,
        private confirmDialog: ConfirmDialog,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        //this.dlService.getClfDataAll().subscribe(
        //    data => {
        //        this.items = data;
        //        this.getNameData();
        //    },
        //    err => this.errorAlert.open(err));
        this.dlService.getTrainStatAll().subscribe(
            data => this.trainings = data,
            err => this.errorAlert.open(err));
        this.requestData1.type = 1;
    }
    inProgress: boolean = false;
    load() {
        this.inProgress = true;
        this.dlService.getClfDataAll().subscribe(
            data => {
                this.inProgress = false;
                this.items = data;
                this.getNameData();
            },
            err => this.errorAlert.open(err));
    }

    names: string[] = [];
    items: LabelSentenceData[];
    trainings: TrainStatData[];
    clfRequest: ClfRequsetData = new ClfRequsetData();
    props: number[] = [];
    requestData0: TrainRequestData = new TrainRequestData();
    requestData1: TrainRequestData = new TrainRequestData();

    languages = ['English', 'Korean', 'Chinese'];

    getNameData() {
        this.dlService.getTrainDataList().subscribe(
            arr => this.names = arr,
            err => this.errorAlert.open(err));
    }

    createData() {
        this.dialog.open(LabeledSentenceDialog)
            .afterClosed().subscribe(ls => {
                if (ls)
                    this.dlService.saveClfData([ls])
                        .subscribe(
                        idArr => {
                            ls._id = idArr[0];
                            this.items.push(ls);
                        }, err => this.errorAlert.open(err)
                        );
            })
    }

    editData(id: number) {
        const ref = this.dialog.open(LabeledSentenceDialog);
        ref.componentInstance.ls = Object.assign(new LabelSentenceData(), this.items[id]);
        ref.afterClosed().subscribe(data => {
            if (data)
                this.dlService.editClfData(data)
                    .subscribe(
                    () => {
                        for (let prop in data)
                            this.items[id][prop] = data[prop];
                    }, err => this.errorAlert.open(err)
                    );
        });
    }

    deleteData(id: number) {
        if (this.items[id]._id >= 0)
            this.dlService.deleteClfData(this.items[id]._id)
                .subscribe(
                () => this.items.splice(id, 1),
                err => this.errorAlert.open(err));
    }

    resetRequest() {
        this.clfRequest = new ClfRequsetData();
    }

    predict() {
        this.dlService.predictClf(this.clfRequest)
            .subscribe(arr => this.props = arr,
            err => this.errorAlert.open(err));
    }

    resetTrainRequest(id: number) {
        if (id == 0)
            this.requestData0 = new TrainRequestData();
        else if (id == 1) {
            this.requestData1 = new TrainRequestData();
            this.requestData1.type = 1;
        }
    }

    executeTraining(data: TrainRequestData) {
        this.dlService.trainClf(data)
            .subscribe(() => this.confirmDialog.open('Train 시작됨'),
            err => this.errorAlert.open(err));
    }
}