﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AIscMaterialModule } from './material.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FlexLayoutModule } from '@angular/flex-layout';
import 'hammerjs';

import { App } from './app';
import { AppRouting } from './app.routing';

import { Login } from './login';
import { Signup } from './signup';
import { Home } from './home';
import { TextClassification } from './text-classification';
import {UserInfo} from './user-info';
import { UserService } from './user.service';
import { DlService } from './dl.service';

import { LoggedIn, NotLoggedIn, IsAdmin } from './check.login';
import { ErrorDialog, ErrorAlert } from './error.alert';
import { ConfirmDialog, ConfirmDialogTemplate } from './confirm.dialog';
import { LabeledSentenceDialog } from './labeledsentence.dialog';

@NgModule({
    imports: [
        BrowserModule, FormsModule, HttpClientModule,
        BrowserAnimationsModule, AIscMaterialModule,
        NgxChartsModule,
        FlexLayoutModule,
        AppRouting
    ],
    declarations: [
        App, Login, Signup,
        Home, UserInfo, TextClassification,
        ErrorDialog, ConfirmDialogTemplate, LabeledSentenceDialog
    ],
    providers: [UserService, LoggedIn, NotLoggedIn, IsAdmin, ErrorAlert,
        ConfirmDialog, DlService
    ],
    bootstrap: [App],
    entryComponents: [
        ErrorDialog, ConfirmDialogTemplate, LabeledSentenceDialog
    ]
})
export class AppModule { }