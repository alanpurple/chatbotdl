﻿import { Routes, RouterModule } from '@angular/router';

import { LoggedIn, NotLoggedIn, IsAdmin } from './check.login';

import { Login } from './login';
import { Signup } from './signup';
import { Home } from './home';
import { UserInfo } from './user-info';
import { TextClassification } from './text-classification';

const appRoutes: Routes = [
    {
        path: '', component: Home
    },
    {
        path: 'login', component: Login,
        canActivate: [NotLoggedIn]
    },
    {
        path: 'login/:id', component: Login,
        canActivate: [NotLoggedIn],
        canDeactivate: [LoggedIn]
    },
    {
        path: 'signup', component: Signup,
        canActivate: [NotLoggedIn]
    },
    {
        path: 'user-info', component: UserInfo
    },
    {
        path: 'text-classification', component: TextClassification
    }
];

export const AppRouting = RouterModule.forRoot(appRoutes);