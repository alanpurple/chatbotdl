﻿import {Injectable} from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { UserData } from './userData';

@Injectable()
export class NotLoggedIn implements CanActivate {
    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    canActivate() {
        return this.http.get('/account/notLoggedIn', { responseType: 'text' })
            .map(res => true)
            .catch(err => {
                if (err.status == 400)
                    return Observable.throw('logged in');
                else
                    return Observable.throw(err);
            });
    }
}

@Injectable()
export class LoggedIn implements CanActivate {
    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    canActivate() {
        return this.http.get<UserData>('/account/info')
            .map(res => true).catch(err => {
                if (err.status != 401) {
                    console.error(err._body);
                    return Observable.throw(err);
                }
                else {
                    this.router.navigate(['/login']);
                    return Observable.throw('not logged in');
                }
            });
    }
}

@Injectable()
export class IsAdmin implements CanActivate {
    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    canActivate() {
        return this.http.get('/account/admin', { responseType: 'text' })
            .map(res => true)
            .catch(err => Observable.throw(err));
    }
}