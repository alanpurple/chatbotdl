from mongoengine import Document,StringField,IntField

# This class should be synced to the class in NodeJS side code
class LabeledSentence(Document):
    _id=IntField(min_value=1)
    sentence=StringField(max_length=100,required=True)
    label=IntField(min=0,required=True)
    name=StringField(required=True)