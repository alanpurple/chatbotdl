#! /usr/bin/env python
import tensorflow as tf
import numpy as np
import os
import time
import datetime
from multiprocessing import Process
from . import data_helpers
from .text_cnn_model import text_cnn_model
from tensorflow.contrib import learn
from .cnn_sentiment import FLAGS
from mongoengine import connect
from .labeled_sentence import LabeledSentence
from train_stat import TrainStat

FILTER_SIZES=[3,4,5]
embedding_dim=128
batch_size=64

def empty_split(iterator):
    return (elem.split(' ') for elem in iterator)

def train(name=None,type=0,fileName=None,userId=None,language=0,
          numEpochs=1,isTokenized=True,maxLength=30):
    # Data Preparation
    # ==================================================
    # Load data
    print("Loading data...")
    if type==0:
        data=pd.read_csv('../data/'+fileName,'\t',dtype={'label':int})
        name=fileName.split('.')[0]
        y=data['label']
        x_text=data['sentence']
    elif type==1:
        connect('ChatbotDemoTestDB',host='mongodb://localhost')
        data=LabeledSentence.objects(name=name)
        y=[elem.label for elem in data]
        y=np.array(y)
        x_text=[elem.sentence for elem in data]
    else:
        raise ValueError

   

    # Build vocabulary
    if isTokenized:
        tkf=empty_split
    else:
        if language==0:
            tkf=data_helpers.tokenizer_fn_en
        elif language==1:
            tkf=data_helpers.tokenizer_fn_kor
        else:
            raise NotImplementedError('language other than English and Korean is not supported yet')

    vocab_processor = learn.preprocessing.VocabularyProcessor(tokenizer_fn=tkf,
                                                                  max_document_length=maxLength)
    X = np.array(list(vocab_processor.fit_transform(x_text)))

    # Write vocabulary
    vocab_processor.save('./'+name+'_vocab')

    train_input_fn=tf.estimator.inputs.numpy_input_fn(
        x={'words':X},
        y=y,
        batch_size=batch_size,
        num_epochs=numEpochs,
        shuffle=False
        )

    # Build model
    classifier=tf.estimator.Estimator(
        text_cnn_model,
        model_dir='./model/'+name,
        params={
            'vocab_size':len(vocab_processor.vocabulary_),
            'embedding_size':embedding_dim,
            'filter_sizes':FILTER_SIZES,
            'num_filters':FLAGS.num_filters,
            'dropout_rate':FLAGS.dropout_rate,
            'l2_reg_lambda':FLAGS.l2_reg_lambda,
            'seq_len':maxLength
            }
        )
    stat=TrainStat(name=name,numEpochs=numEpochs)
    stat.save()
    #def train_process():
    #    classifier.train(train_input_fn)
    #    final=TrainStat.objects(name=name)
    #    final.finished=True
    #    final.save()


    #p=Process(target=train_process)
    #p.start()
    #return p.pid
    classifier.train(train_input_fn)
    TrainStat.objects(name=name).update(finished=True)

def predict(name,input_text,isTokenized=True):
    vocab_processor = learn.preprocessing.VocabularyProcessor.restore('./'+name+'_vocab')
    x_predict = np.array(list(vocab_processor.transform([input_text])))
    classifier=tf.estimator.Estimator(
        text_cnn_model,
        model_dir='./model/'+name,
        params={
            'vocab_size':len(vocab_processor.vocabulary_),
            'embedding_size':embedding_dim,
            'filter_sizes':FILTER_SIZES,
            'num_filters':FLAGS.num_filters,
            'dropout_rate':FLAGS.dropout_rate,
            'l2_reg_lambda':FLAGS.l2_reg_lambda,
            'seq_len':30
            },
        config=tf.estimator.RunConfig()
        )

    predict_inpun_fn=tf.estimator.inputs.numpy_input_fn(x={'words':x_predict},num_epochs=1,shuffle=False)
    #if ckpt_path is None:
    #    ckpt_path=classifier.latest_checkpoint()
    result = next(classifier.predict(predict_inpun_fn))['prob']
    return result