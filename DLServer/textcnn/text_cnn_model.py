import tensorflow as tf
import numpy as np
from .cnn_sentiment import FLAGS

WORDS_FEATURE='words' # Name of the input words feature.
MAX_LABEL=13

"""
A CNN for text classification.
Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
"""
def text_cnn_model(mode,features,labels,params):

    # Embedding layer
    word_vectors=tf.contrib.layers.embed_sequence(
        features[WORDS_FEATURE],params['vocab_size'],params['embedding_size'])
    word_vectors = tf.expand_dims(word_vectors, 3)

    # Create a convolution + maxpool layer for each filter size
    pooled_outputs = []
    for i, filter_size in enumerate(params['filter_sizes']):
        # Convolution Layer
        conv=tf.layers.conv2d(
            word_vectors,
            params['num_filters'],[filter_size,params['embedding_size']],
            activation=tf.nn.relu)
        # Maxpooling over the outputs
        pooled=tf.layers.max_pooling2d(
            conv,
            pool_size=[params['seq_len']-filter_size+1,1],
            strides=1)
        pooled_outputs.append(pooled)

    # Combine all the pooled features
    num_filters_total = params['num_filters'] * len(params['filter_sizes'])
    h_pool = tf.concat(pooled_outputs, 3)
    h_pool_flat = tf.reshape(h_pool, [-1, num_filters_total])

    # Add dropout
    h_drop=tf.layers.dropout(h_pool_flat,params['dropout_rate'])
    # Final (normalized) scores and predictions #gc mod
    scores=tf.contrib.layers.fully_connected(h_drop,MAX_LABEL,scope='scores')
    l2_loss=tf.losses.get_regularization_loss('scores','l2_loss')
    predicted_classes = tf.argmax(scores, 1) #gc mod
    if mode==tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode,predictions={
            'class': predicted_classes,
            'prob': tf.nn.softmax(scores)
        })

    # Calculate mean cross-entropy loss
    onehot_labels = tf.one_hot(labels, MAX_LABEL, 1, 0)
    losses=tf.losses.softmax_cross_entropy(onehot_labels,scores)
    loss = tf.reduce_mean(losses) + params['l2_reg_lambda'] * l2_loss
    if mode==tf.estimator.ModeKeys.TRAIN:
            # Define Training procedure
        optimizer = tf.train.AdamOptimizer(1e-3)
        train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode,loss=loss,train_op=train_op)
    elif mode==tf.estimator.ModeKeys.EVAL:
        eval_metric_ops={
            'accuracy':tf.metrics.accuracy(
                labels=labels,predictions=predicted_classes
                )}
        return tf.estimator.EstimatorSpec(mode,loss=loss,eval_metric_ops=eval_metric_ops)
    
    else:
        raise AttributeError('Unknown mode for estimator')
