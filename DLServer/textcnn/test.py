import tensorflow as tf
from estimator import train_and_test,predict

def main(_):
    ckpt_name='test.ckpt'
    train_and_test(ckpt_name,30)
    result=predict('this is not a good day',ckpt_name)
    print(result)
    for idx,elem in enumerate(result):
        print(elem)

if __name__=='__main__':
    tf.app.run(main=main)