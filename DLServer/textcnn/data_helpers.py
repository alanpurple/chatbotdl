import numpy as np
import pandas as pd
import re
import itertools
from collections import Counter
# Used only for pos(actually this is written in Java, this is silly and unstable)
# must be replaced by native C++ module as soon as possible
# from konlpy.tag import Twitter


def clean_str_en(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()

# temporary
def tokenizer_fn_en(iterator):
    return (clean_str_en(elem).split(' ') for elem in iterator)

# tokenize korean text (alan)
# extremely unstable, not for actual service
def tokenizer_fn_kor(iterator):
    # twt=Twitter()
    # return ([elem[0] for elem in twt.pos(x) if elem[1] in ['Noun','Verb','Adjective','Adverb']] for x in iterator)
    return (elem.split(' ') for elem in iterator)
#gc single data file with (header) (id) text label
# data_file should be tab separated file with header ( sentence and label )
def load_data_and_labels(data_file):
    """
    Loads data from files, splits the data into words and labels.
    Returns split sentences and labels.
    """
    # Load data from files
    data=pd.read_csv(data_file,'\t',dtype={'label':int})
    labels=data['label']
    sentences=data['sentence']
    return sentences,labels