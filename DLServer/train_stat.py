from mongoengine import Document,StringField,IntField,BooleanField

# This class should be synced to the class in NodeJS side code
class TrainStat(Document):
    name=StringField(max_length=20,unique=True,required=True)
    numEpochs=IntField(min_value=1,default=1)
    finished=BooleanField(default=False)