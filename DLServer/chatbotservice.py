import chatbotservice_pb2
import chatbotservice_pb2_grpc
from retrieval import udc_predict
from textcnn import estimator

class ChatbotService(chatbotservice_pb2_grpc.ChatbotServiceServicer):
    def rtTrain(self, request, context):
        return super().rtTrain(request, context)
    def retrieval(self, request, context):
        return super().retrieval(request, context)
    def clfTrain(self, request, context):
        # a name for train folder is required
        if request.name=='':
            return chatbotservice_pb2.TrainResponse(error=0)
        numEpochs=1
        if request.numEpochs!=0:
            numEpochs=request.numEpochs
        # pid for training process, can be used later(or not)
        if request.type==0:
            if request.fileName=='':
                return chatbotservice_pb2.TrainResponse(error=0)
            else:
                pid=estimator.train(fileName=request.fileName,userId=request.userId,
                                    language=request.language,numEpochs=numEpochs,
                                    isTokenized=request.isTokenized,
                                    maxLength=request.maxLength)
        elif request.type==1:
            pid=estimator.train(request.name,type=1,userId=request.userId,
                                language=request.language,numEpochs=numEpochs,
                                isTokenized=request.isTokenized,
                                maxLength=request.maxLength)
        else:
            return chatbotservice_pb2.TrainResponse(error=0)
        return chatbotservice_pb2.TrainResponse(error=-1)
    def textClf(self, request, context):
        if request.sentence=='' or request.name=='':
            return chatbotservice_pb2.ClfResponse(error=0)
        result=estimator.predict(request.name,request.sentence,request.isTokenized)
        return chatbotservice_pb2.ClfResponse(error=-1,probability=result)
    def speech(self, request, context):
        return super().speech(request, context)