import grpc
from concurrent import futures
import time
from chatbotservice import ChatbotService
from chatbotservice_pb2_grpc import add_ChatbotServiceServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

add_ChatbotServiceServicer_to_server(ChatbotService(),server)

server.add_insecure_port('[::]:50051')
server.start()
try:
  while True:
    time.sleep(_ONE_DAY_IN_SECONDS)
except KeyboardInterrupt:
  server.stop(0)