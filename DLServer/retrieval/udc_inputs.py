import tensorflow as tf

TEXT_FEATURE_SIZE = 160

def create_input_fn(mode, input_files, batch_size, num_epochs):
  def input_fn():
     '''
     features = tf.contrib.layers.create_feature_spec_for_parsing(
         get_feature_columns(mode))

     feature_map = tf.contrib.learn.io.read_batch_features(
        file_pattern=input_files,
        batch_size=batch_size,
        features=features,
        reader=tf.TFRecordReader,
        randomize_input=True,
        num_epochs=num_epochs,
        queue_capacity=200000 + batch_size * 10,
        name="read_batch_features_{}".format(mode))
     '''
     def parser(record):
         keys_to_feature={
             'context':tf.FixedLenFeature((TEXT_FEATURE_SIZE),tf.int64),
             'utterance':tf.FixedLenFeature((TEXT_FEATURE_SIZE),tf.int64)
             }
         if mode==tf.estimator.ModeKeys.EVAL:
             for i in range(9):
                 keys_to_feature['distractor_{}'.format(i)]=tf.FixedLenFeature((TEXT_FEATURE_SIZE),tf.int64)
             parsed=tf.parse_single_example(record,keys_to_feature)
             for i in range(9):
                 parsed['distractor_{}_len'.format(i)]=tf.size(parsed['distractor_{}'.format(i)])
             label=tf.zeros([batch_size], dtype=tf.int64)
         elif mode==tf.estimator.ModeKeys.TRAIN:
             keys_to_feature['label']=tf.FixedLenFeature((1),tf.int64)
             parsed=tf.parse_single_example(record,keys_to_feature)
             label=parsed.pop('label')
         parsed['context_len']=tf.size(parsed['context'])
         parsed['utterance_len']=tf.size(parsed['utterance'])
         return parsed,label
     # This is an ugly hack because of a current bug in tf.learn
     # During evaluation TF tries to restore the epoch variable which isn't defined during training
     # So we define the variable manually here
     '''
     if mode == tf.estimator.ModeKeys.TRAIN:
         tf.get_variable(
             "read_batch_features_eval/file_name_queue/limit_epochs/epochs",
             initializer=tf.constant(0, dtype=tf.int64))
         target = feature_map.pop("label")
     else:
        # In evaluation we have 10 classes (utterances).
        # The first one (index 0) is always the correct one
        target = tf.zeros([batch_size, 1], dtype=tf.int64)
     return feature_map, target
     '''
     dataset=tf.data.TFRecordDataset(input_files)
     dataset=dataset.map(parser)
     dataset=dataset.batch(batch_size)
     dataset=dataset.repeat(num_epochs)
     iterator=dataset.make_one_shot_iterator()

     feature,labels=iterator.get_next()
     return feature,labels
  return input_fn
