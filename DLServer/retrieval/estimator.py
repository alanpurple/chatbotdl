import tensorflow as tf
from .udc_model import retrieval_model
from train_stat import TrainStat

def empty_split(iterator):
    return (elem.split(' ') for elem in iterator)

def train(name=None,type=0,fileName=None,userId=None,language=0,
          numEpochs=1,isTokenized=True,max_context_len=160,max_utterance_len=80):
    if type==0:
        pass
    elif type==1:
        pass
    else:
        raise ValueError
    classifier=tf.estimator.Estimator(
        retrieval_model,
        model_dir='./model/'+name,
        params={
            'max_context_len':max_context_len,
            'max_utterance_len':max_utterance_len
            }
        )
    stat=TrainStat(name=name,numEpochs=numEpochs)
    stat.save()

def predict(name,input_text,isTokenized=True):
    vocab_processor = learn.preprocessing.VocabularyProcessor.restore('./'+name+'_vocab')