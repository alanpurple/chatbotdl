import tensorflow as tf
import sys

def get_id_feature(features, key, len_key, max_len):
  ids = features[key]
  ids_len = features[len_key]
  ids_len = tf.minimum(ids_len, tf.constant(max_len, dtype=tf.int32))
  return ids, ids_len

def create_train_op(loss, hparams):
  train_op = tf.contrib.layers.optimize_loss(
      loss=loss,
      global_step=tf.train.get_global_step(),
      learning_rate=hparams.learning_rate,
      clip_gradients=10.0,
      optimizer=hparams.optimizer)
  return train_op


def retrieval_model(mode,features, labels,params):
    context, context_len = get_id_feature(
        features, "context", "context_len", hparams.max_context_len)

    utterance, utterance_len = get_id_feature(
        features, "utterance", "utterance_len", hparams.max_utterance_len)

    if mode == tf.estimator.ModeKeys.TRAIN:
        probs, loss = model_impl(
            hparams,
            mode,
            context,
            context_len,
            utterance,
            utterance_len,
            labels)
        train_op = create_train_op(loss, hparams)
        eval_metrics = {}
        return tf.estimator.EstimatorSpec(mode=mode,loss=loss,train_op=train_op)

    elif mode == tf.estimator.ModeKeys.PREDICT:
        all_contexts = [context]
        all_context_lens = [context_len]
        all_utterances = [utterance]
        all_utterance_lens = [utterance_len]
        for i in range(1,features["len"]):
            distractor, distractor_len = get_id_feature(features,
                "utterance_{}".format(i),
                "utterance_{}_len".format(i),
                hparams.max_utterance_len)
            all_contexts.append(context)
            all_context_lens.append(context_len)
            all_utterances.append(distractor)
            all_utterance_lens.append(distractor_len)
        probs, loss = model_impl(
            hparams,
            mode,
            tf.concat(all_contexts,0),
            tf.concat(all_context_lens,0),
            tf.concat(all_utterances,0),
            tf.concat(all_utterance_lens,0),
            None)
        #split_probs = tf.split(probs, features["len"], 0)
        #shared_probs = tf.concat(split_probs,1)
        return tf.estimator.EstimatorSpec(mode=mode,predictions=probs)

    elif mode == tf.estimator.ModeKeys.EVAL:
        batch_size = hparams.eval_batch_size
        # We have 10 exampels per record, so we accumulate them
        all_contexts = [context]
        all_context_lens = [context_len]
        all_utterances = [utterance]
        all_utterance_lens = [utterance_len]
        all_targets = [tf.ones([batch_size, 1], dtype=tf.int64)]

        for i in range(9):
            distractor, distractor_len = get_id_feature(features,
            "distractor_{}".format(i),
            "distractor_{}_len".format(i),
            hparams.max_utterance_len)
            all_contexts.append(context)
            all_context_lens.append(context_len)
            all_utterances.append(distractor)
            all_utterance_lens.append(distractor_len)
            all_targets.append(
                tf.zeros([batch_size, 1], dtype=tf.int64)
            )

        probs, loss = model_impl(
            hparams,
            mode,
            tf.concat(all_contexts,0),
            tf.concat(all_context_lens,0),
            tf.concat(all_utterances,0),
            tf.concat(all_utterance_lens,0),
            tf.concat(all_targets,0))

        split_probs = tf.split(probs,10,0)
        shaped_probs = tf.concat(split_probs,1)

        # Add summaries
        tf.summary.histogram("eval_correct_probs_hist", split_probs[0])
        tf.summary.scalar("eval_correct_probs_average", tf.reduce_mean(split_probs[0]))
        tf.summary.histogram("eval_incorrect_probs_hist", split_probs[1])
        tf.summary.scalar("eval_incorrect_probs_average", tf.reduce_mean(split_probs[1]))

        eval_metrics = {}
        for k in [1, 2, 5, 10]:
            eval_metrics["recall_at_%d" % k] = tf.metrics.recall_at_k(labels,shaped_probs,k)
        return tf.estimator.EstimatorSpec(mode,shaped_probs,loss,eval_metric_ops=eval_metrics)