import os
import time
import sys
import numpy as np
import tensorflow as tf
import pandas as pd
#from konlpy.tag import Twitter
from . import udc_model
from . import udc_hparams
from . import udc_metrics
from . import udc_inputs
from .dual_encoder import dual_encoder_model

tf.flags.DEFINE_string("model_dir", None, "Directory to load model checkpoints from")
tf.flags.DEFINE_string("vocab_processor_file", "./data/vocab_processor.bin", "Saved vocabulary processor file")
FLAGS = tf.flags.FLAGS

#twt=Twitter()
#def tokenizer_fn(iterator):
#    return ([elem[0] for elem in twt.pos(x) if elem[1] in ['Noun','Verb','Adjective','Adverb']] for x in iterator)
def tokenizer_fn(iterator):
    return (elem.split(' ') for elem in iterator)

# Question
#INPUT_CONTEXT='what is happening'
## Candidate responses
#responses=pd.read_csv('./data/response.csv',delimiter='\r',header=None)
#POTENTIAL_RESPONSES=responses.iloc[:][0]

def get_features(context, utterances):
  context_matrix = np.array(list(vp.transform([context])))
  utterance_matrix = np.array(list(vp.transform([utterances[0]])))
  context_len = len(twt.pos(context))
  utterance_len = len(twt.pos(utterances[0]))
  print(len(utterances))
  print(utterance_len)
  features = {
    "context": tf.convert_to_tensor(context_matrix, dtype=tf.int64),
    "context_len": tf.constant(context_len, shape=[1,1], dtype=tf.int32),
    "utterance": tf.convert_to_tensor(utterance_matrix, dtype=tf.int64),
    "utterance_len": tf.constant(utterance_len, shape=[1,1], dtype=tf.int32),
    "len":len(utterances)
  }

  for i in range(1,len(utterances)):
      utterance = utterances[i];

      utterance_matrix = np.array(list(vp.transform([utterance])))
      utterance_len = len(twt.pos(utterance))

      features["utterance_{}".format(i)] = tf.convert_to_tensor(utterance_matrix, dtype=tf.int64)
      features["utterance_{}_len".format(i)] = tf.constant(utterance_len, shape=[1,1], dtype=tf.int32)

  return features, None

def predict(input,isTokenized=False):
    # Load vocabulary
    vp = tf.contrib.learn.preprocessing.VocabularyProcessor.restore(
    FLAGS.vocab_processor_file)
    hparams = udc_hparams.create_hparams()
    model_fn = udc_model.create_model_fn(hparams, model_impl=dual_encoder_model)
    estimator = tf.estimator.Estimator(model_fn=model_fn, model_dir=FLAGS.model_dir,
    config=tf.estimator.RunConfig())

    #starttime = time.time()

    prob = estimator.predict(input_fn=lambda: get_features(INPUT_CONTEXT, POTENTIAL_RESPONSES))
    results = next(prob)

    #endtime = time.time()

    #print('\n')
    #print("[Predict time]%.2f sec" % round(endtime - starttime,2))
    #print("[     Context]",INPUT_CONTEXT)
    # print("[Results value ]",results)
    return results.argmax(axis=0)
    #print(results[0])
    #print(answerId)
    #print(results[answerId])
    #results.sort()
    #print(results[-20:])
    #print('[      Answer]', POTENTIAL_RESPONSES[answerId])