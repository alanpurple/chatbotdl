# Test platform for Tensorflow Modules

## Preparation for testing
* Node.js 8.9 or above (latest one is recommended)
* NPM(included in node.js) and npm packages(Typescript and Gulpjs)
* Anaconda 5.0.1 or above
* Tensorflow-gpu 1.4 or above
* CUDA 8 or above
* cuDNN 6 or above
* python packages - grpc, scikit-learn, etc ( many of them are included in Anaconda)

## Actual process
* "npm install -g typescipt", "npm install -g gulp"
* "npm install" in "ChatbotWebServer" and in "ChatbotDemo"
* type "gulp copy" in "ChatbotDemo"
* type "tsc" in ChatbotDemo
* need grpc module(chatbotservice.proto) compile see tutorial ( https://grpc.io/docs/tutorials/basic/python.html )
* In "DLServer", type "python DLServer.py", then deep learning server instance started
* In "ChatbotWebServer", type "node app", then Web server started
* Web server in localhost should be started with port 3000 if the port is not specified explicitly

## Caution
* Training data section shows all data without page(should be implemented later),
  so might take long to render

### Retrieval parameters
* Retrieval model parameters are not all set

### How to use
* The server imports Playchat data first only once, if you don't need this data, just comment that line
* All training data has 'name' property which is a group name for training
* Once trained, you can use(predict) it from the third tab